package com.example.ECommerce.utilty;

import com.example.ECommerce.Entity.Product;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@Setter
@Getter
public class InMemoryCache {
    @Autowired
    public static final Map<UUID, Product> productMap= new HashMap<>();
    static {
        Product product = new Product();
        product.setPrice(100);
        product.setDescription("dummy product");
        productMap.put(UUID.randomUUID(),product);
    }

}
