package com.example.ECommerce.controller;

import com.example.ECommerce.Entity.Product;
import com.example.ECommerce.Service.ProductDao;
import com.example.ECommerce.dto.ProductModel;
import com.example.ECommerce.dto.UpdateProductModel;
import com.example.ECommerce.dto.UuidModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class ProductController {
    @Autowired
    ProductDao productDao;

    @PostMapping("/create")
    public ResponseEntity<Product> create(@RequestBody ProductModel productModel){
        Product product = productDao.create(productModel);
        return ResponseEntity.ok(product);
    }
    @PostMapping("/read")
    public ResponseEntity<Product> read(@RequestBody UuidModel uuidModel){
        System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::: "+uuidModel.getUuid());
        UUID product = UUID.fromString(uuidModel.getUuid());
        return productDao.read(product);
    }
    @PutMapping("/update")
    public ResponseEntity<String> update(@RequestBody UpdateProductModel updateProductModel){
        return productDao.update(updateProductModel);
    }
    @DeleteMapping("/delete")
    public ResponseEntity<String> delete(@RequestBody UuidModel uuidModel){
        UUID id = UUID.fromString(uuidModel.getUuid());
        return productDao.delete(id);
    }

}
