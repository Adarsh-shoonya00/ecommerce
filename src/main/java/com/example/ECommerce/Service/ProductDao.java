package com.example.ECommerce.Service;

import com.example.ECommerce.Entity.Product;
import com.example.ECommerce.dto.ProductModel;
import com.example.ECommerce.dto.UpdateProductModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface ProductDao {
    public Product create(ProductModel productModel);
    public ResponseEntity<Product> read(UUID productId);
    public ResponseEntity<String> update(UpdateProductModel updateProductModel);
    public ResponseEntity<String> delete(UUID id);

}
