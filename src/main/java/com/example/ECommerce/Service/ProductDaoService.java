package com.example.ECommerce.Service;

import com.example.ECommerce.Entity.Product;
import com.example.ECommerce.dto.ProductModel;
import com.example.ECommerce.dto.UpdateProductModel;
import com.example.ECommerce.utilty.InMemoryCache;
import com.example.ECommerce.utilty.PriceCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ProductDaoService implements ProductDao{


    @Override
    public Product create(ProductModel productModel) {
        Product product = new Product();
        product.setName(productModel.getName());
        if(productModel.getDiscount() == 0 && productModel.getTax() ==0) {
            product.setPrice(productModel.getPrice());
        }
        else{
            PriceCalculator priceCalculator = new PriceCalculator();
            double finalPrice = priceCalculator.finalPriceCalculator(productModel.getTax(), productModel.getDiscount(), productModel.getPrice());
            System.out.println("::::::::::::::::::::::::::::::::::::::::::::::: dao service:::::::::: "+finalPrice);
            product.setPrice(finalPrice);
        }
        product.setDescription(productModel.getDesc());
        product.setQuantityAvailable(productModel.getQuantityAvailable());
        product.setProductId(UUID.randomUUID());
        System.out.println("::::::::::::::::::::::::::::::::::::::::: "+ product.getProductId());
        InMemoryCache.productMap.put(product.getProductId(),product);
        Product productReturn = InMemoryCache.productMap.get(product.getProductId());
        return productReturn;

    }

    @Override
    public ResponseEntity<Product> read(UUID productId) {
        try{
            Product product = InMemoryCache.productMap.get(productId);
            return ResponseEntity.ok(product);
        }
        catch(Exception e){
            Product product = null;
            return ResponseEntity.internalServerError().body(product);

        }
    }

    @Override
    public ResponseEntity<String> update(UpdateProductModel updateProductModel) {
        try {
            UUID id = updateProductModel.getId();
            Product product = InMemoryCache.productMap.get(id);
            if (updateProductModel.getDesc() != null) {
                product.setDescription(updateProductModel.getDesc());
            }
            if (updateProductModel.getName() != null)
                product.setName(updateProductModel.getName());
            if (updateProductModel.getPrice() > 0)
                product.setPrice(updateProductModel.getPrice());
            if (updateProductModel.getQuantityAvailable() >= 0)
                product.setQuantityAvailable(updateProductModel.getQuantityAvailable());
            InMemoryCache.productMap.put(id, product);
            return ResponseEntity.ok("Success");
        }
        catch (Exception e){
            return ResponseEntity.ok("failure ");
        }
    }

    @Override
    public ResponseEntity<String> delete(UUID id) {
        try{
            InMemoryCache.productMap.remove(id);
            return ResponseEntity.ok("Success");
        }
        catch (Exception e){
            return ResponseEntity.ok("Failed to delete");
        }
    }


}
