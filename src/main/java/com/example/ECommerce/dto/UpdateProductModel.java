package com.example.ECommerce.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class UpdateProductModel {
    UUID id;
    String name;
    String desc;
    int price;
    int quantityAvailable;
}
