package com.example.ECommerce.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductModel {
    String name;
    String desc;
    double price;
    int quantityAvailable;
    int tax;
    int discount;
}
