package com.example.ECommerce.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class UuidModel {
    String uuid;
}
