package com.example.ECommerce.Entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Product {
    @Id
    private UUID productId;
    String name;
    double price;
    String description;
    int QuantityAvailable;
}
