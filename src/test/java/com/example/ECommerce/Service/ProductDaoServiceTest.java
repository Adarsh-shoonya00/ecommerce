package com.example.ECommerce.Service;

import com.example.ECommerce.Entity.Product;
import com.example.ECommerce.dto.ProductModel;
import com.example.ECommerce.utilty.InMemoryCache;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductDaoServiceTest {
    @Autowired
    ProductDaoService productDaoService;

    @Test
    void create() {
        ProductModel productModel = new ProductModel();
        productModel.setDesc("nike");
        productModel.setPrice(100);
        productModel.setName("jordan");
        productModel.setQuantityAvailable(10);
        productModel.setTax(0);
        productModel.setDiscount(0);
        Product productActual = new Product();
        productActual.setQuantityAvailable(10);
        productActual.setDescription("nike");
        productActual.setName("jordan");
        productActual.setPrice(100);
        Product productExpected = productDaoService.create(productModel);

        if(productExpected.getDescription().equals(productActual.getDescription())){
            assertTrue(true);

        }
    }
    @Test
    void createWithTax(){
        ProductModel productModel = new ProductModel();
        productModel.setDesc("nike");
        productModel.setPrice(100);
        productModel.setName("jordan");
        productModel.setQuantityAvailable(10);
        productModel.setTax(10);
        productModel.setDiscount(0);
        Product productActual = new Product();
        productActual.setQuantityAvailable(10);
        productActual.setDescription("nike");
        productActual.setName("jordan");
        productActual.setPrice(100);
        Product productExpected = productDaoService.create(productModel);

        if(productExpected.getDescription().equals(productActual.getDescription())){
            assertTrue(true);

        }

    }

    @Test
    void read() {


    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}